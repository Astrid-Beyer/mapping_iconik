<?php 
/**
 * @package  IconikPlugin
 */
namespace Inc\Base;

use Inc\Api\SettingsApi;
use Inc\Base\BaseController;
use Inc\Api\Callbacks\MapCallbacks;
use Inc\Api\Callbacks\MapResultCallbacks;
use Inc\Api\Callbacks\AdminCallbacks;

/**
* 
*/
class MapController extends BaseController {
	public $settings;
	public $callbacks;
	public $map_callbacks;
	public $subpages = array();
	public $map = array();

	public function register() {
		// if ( ! $this->activated( 'map_manager' ) ) return;

		$this->settings = new SettingsApi();
		$this->callbacks = new AdminCallbacks();
		$this->map_callbacks = new MapCallbacks();
		$this->mapping_result_callbacks = new MapResultCallbacks();

		$this->setSubpages();

		$this->settings->addSubPages( $this->subpages )->register();

		if ( ! empty( $this->map ) ) {
			add_action( 'init', array( $this, 'map' ) );
		}
		add_action('wp_ajax_add_map', 		array($this->map_callbacks, 'addRoom'));
		add_action('wp_ajax_delete_map', 	array($this->map_callbacks, 'deleteRoom'));
		add_action('wp_ajax_get_map', 		array($this->map_callbacks, 'getRoom'));
		add_action('wp_ajax_edit_map', 		array($this->map_callbacks, 'editRoom'));

		$this->setShortCodePage(); //setup & activate shortcode

		add_shortcode('ico_mapping_result', array($this->mapping_result_callbacks, 'ico_mapping_result'));
	}


	public function setSubpages() {
		$this->subpages = array(
			array(
				'parent_slug' => 'iconik_plugin', 
				'page_title' => 'Mapping', 
				'menu_title' => 'Map Manager',
				'capability' => 'manage_options', 
				'menu_slug' => 'iconik_map', 
				'callback' => array( $this->callbacks, 'adminMap' )
			)
		);
	}

	public function setShortCodePage() {
		$subPage = array(
			array(
				'parent_slug' => 'admin.php?page=iconik_map',
				'page_title' => 'Shortcodes',
				'menu_title' => 'Shortcodes',
				'capability' => 'manage_options',
				'menu_slug' => 'iconik_shortcode',
				'callback' => array($this->mapping_result_callbacks, 'shortcodePage')
			)
		);
		
		$this->settings->addSubPages($subPage)->register();
	}
	
}