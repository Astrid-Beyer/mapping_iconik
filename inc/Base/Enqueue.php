<?php

/**
 * @package iconikPlugin
 */

namespace Inc\Base;

use Inc\Base\BaseController;

class Enqueue extends BaseController {
	public function register() {
		add_action('admin_enqueue_scripts', array($this, 'enqueue'));
		add_action('wp_enqueue_scripts', array($this, 'enqueue_front'));
		
		add_action( 'wp_enqueue_scripts', array($this, 'custom_shortcode_scripts'));
	}

	function enqueue() {
		// enqueue all our scripts
		wp_enqueue_style('mypluginstyle', $this->plugin_url . 'node_modules/bulma/css/bulma.min.css');
		wp_enqueue_style('font-awesome-6', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css', array(), null);
		wp_enqueue_style('animate', 'https://cdn.jsdelivr.net/npm/animate.css@4.0.0/animate.min.css', array(), null);
		wp_enqueue_script('font-awesome-6-js', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/all.min.js');
		// wp_enqueue_style( 'mypluginstyle', $this->plugin_url . 'assets/mystyle.css' );
		wp_enqueue_script('mypluginscript', $this->plugin_url . 'assets/js/scriptAdmin.js', array('jquery'), false, true);
		wp_enqueue_script('bulma-toast', 'https://cdn.jsdelivr.net/npm/bulma-toast@2.4.1/dist/bulma-toast.min.js');
		wp_localize_script( 'mypluginscript', 'scriptData', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );
		wp_localize_script( 'mypluginscript', 'scriptData', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'mapApiKey' => '1f9311198a9da17a343c1dd6255ecdcc') );
	}

	
	function enqueue_front() {
		wp_enqueue_style('font-awesome-6', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css', array(), null);
		wp_enqueue_style('animate', 'https://cdn.jsdelivr.net/npm/animate.css@4.0.0/animate.min.css', array(), null);
		wp_enqueue_script('font-awesome-6-js', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/all.min.js');
		wp_enqueue_style('mapbox-css', 'https://api.mapbox.com/mapbox.js/v2.4.0/mapbox.css', array(), null);
		wp_enqueue_script('mapbox-js', 'https://api.mapbox.com/mapbox.js/v2.4.0/mapbox.js');
		wp_enqueue_style( 'mypluginstyle', $this->plugin_url . 'assets/css/styleFront.css' );
		wp_enqueue_script('script-front', $this->plugin_url . 'assets/js/scriptFront.js', array('jquery'));
		wp_enqueue_script('script-front-delay', $this->plugin_url . 'assets/js/scriptFrontDelay.js', array('jquery'), false, true);
		wp_enqueue_style('bulma-css', $this->plugin_url . 'node_modules/bulma/css/bulma.min.css');
		wp_enqueue_script('bulma-toast', 'https://cdn.jsdelivr.net/npm/bulma-toast@2.4.1/dist/bulma-toast.min.js');
		
	}

	function custom_shortcode_scripts() {
		global $post;
		if( is_a( $post, 'WP_Post' ) && (has_shortcode( $post->post_content, 'ico_faq') || has_shortcode( $post->post_content, 'ico_form'))) {
			wp_enqueue_script('media-upload');
			wp_enqueue_media();
			wp_localize_script( 'script-front-delay', 'scriptData', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'recaptcha_site_key' => get_option('ico_site_key_recaptcha') ) );
			wp_enqueue_script('google-reCAPTCHA-v3', 'https://www.google.com/recaptcha/api.js?render=' . get_option('ico_site_key_recaptcha'));
		}
	}
}
