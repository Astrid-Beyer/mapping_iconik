<?php

/**
 * @package iconikPlugin
 */

namespace Inc\Base;

use Inc\Base\BaseController;

class ApiReCaptcha extends BaseController {
	public function register() {
        add_filter("script_loader_tag", array($this,'add_re_captcha_js_key'), 10, 2 );
    }

    function add_re_captcha_js_key( $tag, $handle ) {
        if ( 'google-reCAPTCHA-v3' === $handle ) {
            return str_replace( "SITEKEY", get_option('ico_site_key_recaptcha'), $tag );
        }
        return $tag;
    }
}
