<?php

/**
 * @package iconikPlugin
 */

namespace Inc\Base;

use Inc\Base\BaseController;

class FontAwesome extends BaseController {
	public function register() {
		add_filter("style_loader_tag", array($this, 'kit_fontawesome'), 10, 2 );
        add_filter("script_loader_tag", array($this,'add_font_awesome_js_cdn_attributes'), 10, 2 );
    }

	function kit_fontawesome($html, $handle) {
        if ( 'font-awesome-6' === $handle ) {
            return str_replace( "media='all'", "media='all' integrity='sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==' crossorigin='anonymous' referrerpolicy='no-referrer'", $html );
        }
        return $html;
	}

    function add_font_awesome_js_cdn_attributes( $tag, $handle ) {
        if ( 'font-awesome-6-js' === $handle ) {
            return str_replace( "src", "integrity='sha512-6PM0qYu5KExuNcKt5bURAoT6KCThUmHRewN3zUFNaoI6Di7XJPTMoT6K0nsagZKk2OB4L7E3q1uQKHNHd4stIQ==' crossorigin='anonymous' referrerpolicy='no-referrer' src", $tag );
        }
        return $tag;
    }
}
