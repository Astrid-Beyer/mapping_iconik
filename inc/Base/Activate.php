<?php

/**
 * @package  IconikPlugin
 */

namespace Inc\Base;

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

class Activate {
    public static function activate() {
        flush_rewrite_rules();

        $default = array();

        if (!get_option('iconik_plugin')) {
            update_option('iconik_plugin', $default);
        }

        if (!get_option('iconik_plugin_map')) {
            update_option('iconik_plugin_map', $default);
        }

        // Base de données : wp_mappings
        global $wpdb;
        //$charset_collate = $wpdb->get_charset_collate();
        $mappingTable = $wpdb->base_prefix . "mapping";
        $qaTable = $wpdb->base_prefix . "qa";
        $qaCategoryTable = $wpdb->base_prefix . "qa_category";

        $sql = "CREATE TABLE IF NOT EXISTS $mappingTable (
                `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
                `name` varchar(80),
                `address` varchar(255) NOT NULL,
                `link` varchar(200),
                `latitude` decimal(8,6),
                `longitude` decimal(9,6),
                PRIMARY KEY(`id`)
            ) ENGINE = INNODB
            DEFAULT CHARACTER SET = utf8
            COLLATE = utf8_general_ci;";

        dbDelta($sql);

        $sql1 = "CREATE TABLE IF NOT EXISTS $qaCategoryTable (
                `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(20),
                PRIMARY KEY(`id`)
            ) ENGINE = INNODB
            DEFAULT CHARACTER SET = utf8
            COLLATE = utf8_general_ci;";

        dbDelta($sql1);

        $sql2 = "CREATE TABLE IF NOT EXISTS $qaTable (
            `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
            `question` VARCHAR(300),
            `answer` VARCHAR(1500),
            `category` BIGINT UNSIGNED,
            PRIMARY KEY(`id`),
            FOREIGN KEY (`category`) REFERENCES $qaCategoryTable(`id`)
        ) ENGINE = INNODB
        DEFAULT CHARACTER SET = utf8
        COLLATE = utf8_general_ci;";

        dbDelta($sql2);

        $count = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}qa_category WHERE id IS NOT NULL");

        if ($count == 0) {   // si la table qa_category n'est pas vide      
            $categories = array('Général', 'Oculus', 'Vive', 'PlayStation VR', 'Multijoueur');
            foreach ($categories as $category) {
                $data = array('name' => $category);
                $wpdb->insert($wpdb->prefix . 'qa_category', $data, '%s');
            }
        }


        $is_error = empty($wpdb->last_error);
        return $is_error;
    }
}
