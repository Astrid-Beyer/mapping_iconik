<?php

/**
 * @package  IconikPlugin
 */

namespace Inc\Base;

use Inc\Api\SettingsApi;
use Inc\Base\BaseController;
use Inc\Api\Callbacks\AdminCallbacks;
use Inc\Api\Callbacks\FaqCallbacks;
use Inc\Api\Callbacks\FaqFrontCallbacks;

class FaqController extends BaseController {
    public $settings;
	public $callbacks;
	public $map_callbacks;
	public $subpages = array();
	public $map = array();

    public function register() {
        // if (!$this->activated('faq_manager')) return;

        $this->settings = new SettingsApi();
        $this->callbacks = new AdminCallbacks();
		$this->faq_callbacks = new FaqCallbacks();
		$this->faq_front_callbacks = new FaqFrontCallbacks();

        $this->setSubpages();
        
		$this->settings->addSubPages( $this->subpages )->register();
		
		add_action('wp_ajax_nopriv_contact_form', array($this->faq_front_callbacks, 'reCaptchav3'));
		add_action('wp_ajax_contact_form', array($this->faq_front_callbacks, 'reCaptchav3'));
		add_action('wp_ajax_update_site_key', array($this->faq_front_callbacks, 'updateSiteKey'));
		add_action('wp_ajax_update_secret_key', array($this->faq_front_callbacks, 'updateSecretKey'));
		add_action('wp_ajax_add_qa', array($this->faq_callbacks, 'addQA'));
		add_action('wp_ajax_get_qa', array($this->faq_callbacks, 'getQA'));
		add_action('wp_ajax_edit_qa', array($this->faq_callbacks, 'editQA'));
		add_action('wp_ajax_delete_qa', array($this->faq_callbacks, 'deleteQA'));
		add_action('wp_ajax_add_category', array($this->faq_callbacks, 'addCategory'));
		add_action('wp_ajax_delete_category', array($this->faq_callbacks, 'deleteCategory'));

		$this->setShortCodePage(); //setup & activate shortcode

		add_shortcode('ico_faq', array($this->faq_front_callbacks, 'ico_faq'));
		add_shortcode('ico_form', array($this->faq_front_callbacks, 'ico_form'));
	}

    public function setSubpages() {
		$this->subpages = array(
			array(
				'parent_slug' => 'iconik_plugin', 
				'page_title' => 'FAQ', 
				'menu_title' => 'FAQ', 
				'capability' => 'manage_options', 
				'menu_slug' => 'iconik_faq', 
				'callback' => array( $this->callbacks, 'adminFaq' )
			)
		);
	}

	public function setShortCodePage() {
		$subPage = array(
			array(
				'parent_slug' => 'admin.php?page=iconik_map',
				'page_title' => 'Shortcodes',
				'menu_title' => 'Shortcodes',
				'capability' => 'manage_options',
				'menu_slug' => 'iconik_shortcode',
				'callback' => array($this->faq_front_callbacks, 'shortcodePage')
			)
		);
		
		$this->settings->addSubPages($subPage)->register();
	}
}
