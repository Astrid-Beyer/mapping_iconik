<?php 
/**
 * @package  IconikPlugin
 */
namespace Inc\Pages;

use Inc\Api\SettingsApi;
use Inc\Base\BaseController;
use Inc\Api\Callbacks\AdminCallbacks;
use Inc\Api\Callbacks\ManagerCallbacks;

class Dashboard extends BaseController {
	public $settings;
	public $callbacks;
	public $callbacks_mngr;
	public $pages = array();

	
	public function register() {
		$this->settings = new SettingsApi();
		$this->callbacks = new AdminCallbacks();
		$this->callbacks_mngr = new ManagerCallbacks();

		$this->setPages();
		$this->settings->addPages( $this->pages )->withSubPage( 'Dashboard' )->register();
		// $this->settings->addPages( $this->pages )->addSubPages($this->subpages)->register();
	}

	public function setPages() {
		$this->pages = array(
			array(
				'page_title' => 'Iconik Plugin', 
				'menu_title' => 'Iconik', 
				'capability' => 'manage_options', 
				'menu_slug' => 'iconik_plugin', 
				'callback' => array( $this->callbacks, 'adminDashboard' ), 
				'icon_url' => 'dashicons-store', 
				'position' => 110
			)
		);

	}
}