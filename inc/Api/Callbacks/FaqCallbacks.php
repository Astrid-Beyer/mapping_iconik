<?php

/**
 * @package  IconikPlugin
 */

namespace Inc\Api\Callbacks;

class FaqCallbacks {
    public function addQA() {
		$qaQuestion 	= sanitize_text_field($_POST['wp_qa_question']);
		$qaAnswer 		= stripslashes($_POST['wp_qa_answer']);
		$qaCategory 	= sanitize_text_field($_POST['wp_qa_category']);

		global $wpdb;

		$isSuccess = $wpdb->insert(
			$wpdb->prefix . 'qa',
			array(
				'question' => $qaQuestion,
				'answer'   => $qaAnswer,
				'category' => $qaCategory
			)
		);
		if ($isSuccess) wp_send_json_success($wpdb->insert_id);
		else wp_send_json_error($wpdb->last_error);
		
	}

	public function addCategory() {
		$qaNewCategory 	= sanitize_text_field($_POST['wp_qa_category_name']);

		global $wpdb;
		$isSuccess = $wpdb->insert(
			$wpdb->prefix . 'qa_category',
			array('name' => $qaNewCategory)
		);
		if ($isSuccess) wp_send_json_success($wpdb->insert_id);
		else wp_send_json_error($wpdb->last_error);

	}	
	
	public function deleteCategory() {
		$id = (int) $_POST['wp_category_id'];
		global $wpdb;
		
		$isSuccess = $wpdb->delete(
			$wpdb->prefix . 'qa_category',
			['id' => $id],
			['%d'], // id format (sécurité) 
		);

		if ($isSuccess) wp_send_json_success("success");
		else wp_send_json_error($wpdb->last_error);
	}
	
	public function getQA() {
		$id = $_POST['wp_qa_id'];		
		global $wpdb;
		$results = $wpdb->get_results(
			$wpdb->prepare("
				SELECT * 
				FROM " . $wpdb->prefix . "qa
				WHERE id = " . $id
			)
		);
		if ($results) wp_send_json_success($results);
		else wp_send_json_error($wpdb->last_error);
	}

	public function editQA() {
		$id = (int) $_POST['wp_qa_id'];
		
		$qaQuestion = sanitize_text_field($_POST['wp_qa_question']);
		$qaAnswer 	= stripslashes($_POST['wp_qa_answer']);
		$qaCategory = sanitize_text_field($_POST['wp_qa_category']);

		global $wpdb;
		
		$isSuccess = $wpdb->update($wpdb->prefix . 'qa', 
			array('question' => $qaQuestion, 'answer' => $qaAnswer, 'category' => $qaCategory),
			array('id' => $id)
		);
		if ($isSuccess) wp_send_json_success("success");
		else wp_send_json_error($wpdb->last_error);
	}

	
	public function deleteQA() {
		$id = (int) $_POST['wp_qa_id'];
		global $wpdb;
		
		$isSuccess = $wpdb->delete(
			$wpdb->prefix . 'qa',
			['id' => $id],
			['%d'], // id format (sécurité) 
		);

		if ($isSuccess) wp_send_json_success("success");
		else wp_send_json_error($wpdb->last_error);
	}


}