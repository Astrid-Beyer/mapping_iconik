<?php 
/**
 * @package  IconikPlugin
 */
namespace Inc\Api\Callbacks;

use Inc\Base\BaseController;

class AdminCallbacks extends BaseController {
	public function adminDashboard() {
		return require_once($this->plugin_path . "/Templates/admin.php" );
	}

	public function adminMap() {
		return require_once( $this->plugin_path . "/Templates/map.php" );
	}

	public function adminFaq() {
		return require_once( $this->plugin_path . "/Templates/faq.php" );
	}
}