<?php 
/**
 * @package  IconikPlugin
 */
namespace Inc\Api\Callbacks;

use Inc\Base\BaseController;

class MapResultCallbacks extends BaseController {
    public function ico_mapping_result() {
		ob_start(); // php ne print pas directement
		global $wpdb;
		$str = '';
		$sql = "SELECT * FROM `wp_mapping`";
		$maps = $wpdb->get_results($sql);

		require_once($this->plugin_path . "/Templates/mapping_result.php");
		return ob_get_clean();
	}
    public function shortcodePage() {
        return require_once($this->plugin_path . "/Templates/mapping_result.php");
    }

}