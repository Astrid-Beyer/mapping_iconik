<?php

/**
 * @package  IconikPlugin
 */

namespace Inc\Api\Callbacks;

use Inc\Base\BaseController;

class FaqFrontCallbacks extends BaseController
{

	public function myFileUploader()
	{
		if (isset($_POST['submit'])) {
			wp_upload_bits($_FILES['fileToUpload']['name'], null, file_get_contents($_FILES['fileToUpload']['tmp_name']));
		}
	}

	public function updateSiteKey()
	{
		if (isset($_POST['wp_captcha_site_key'])) {
			$wp_captcha_site_key = sanitize_text_field($_POST['wp_captcha_site_key']);
			update_option('ico_site_key_recaptcha', $wp_captcha_site_key);

			get_option('ico_site_key_recaptcha');
		} else get_option('ico_site_key_recaptcha');
	}

	public function updateSecretKey()
	{
		if (isset($_POST['wp_captcha_secret_key'])) {
			$wp_captcha_secret_key = sanitize_text_field($_POST['wp_captcha_secret_key']);
			update_option('ico_secret_key_recaptcha', $wp_captcha_secret_key);

			get_option('ico_secret_key_recaptcha');
		} else get_option('ico_secret_key_recaptcha');
	}

	public function reCaptchav3()
	{
		if (isset($_POST['g-recaptcha-response'])) {
			$captcha = $_POST['g-recaptcha-response'];
		} else {
			$captcha = false;
		}

		if (!$captcha) {
			wp_send_json_error();
		} else {
			$secret   = get_option('ico_secret_key_recaptcha');
			$response = file_get_contents(
				"https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']
			);
			// use json_decode to extract json response
			$response = json_decode($response);

			if ($response->success === false) {
				wp_send_json_error();
			}

			$contactObj = sanitize_text_field($_POST['contact_obj']);
			$contactFile = $_POST['contact_file'];
			$contactContent = sanitize_text_field($_POST['contact_content']);
			$headers = array('Content-Type: text/html; charset=UTF-8');

			foreach ($contactFile as $image)
				$contactContent .= "<br><img src='$image' />";

			// envoi du mail
			wp_mail("communication@iconik.com", "[" . get_bloginfo('name') . "] $contactObj", $contactContent, $headers);
			wp_send_json_success();
		}

		//... The Captcha is valid you can continue with the rest of your code
		//... Add code to filter access using $response . score
		if ($response->success == true && $response->score <= 0.5) {
			wp_send_json_error();
		}
	}

	public function ico_faq()
	{
		ob_start(); // php ne print pas directement
		global $wpdb;
		$str = '';
		$categories = $wpdb->get_results("SELECT * FROM $wpdb->prefix" . "qa_category");

		foreach ($categories as $cat) {
			$str .= "<div class='category-faq'><h3 class='title is-3 bold'>" . $cat->name . "</h3>";

			$sql = "SELECT * FROM $wpdb->prefix" . "qa" . " WHERE category = " . $cat->id;
			$faq = $wpdb->get_results($sql);

			$str .= "<div class='qa-box'>";

			foreach ($faq as $qa) {
				$str .= "<div class='qa_block'>
					<p id=" . $qa->id . " class='bold qa_question'>" . $qa->question . " <i id='icon-" . $qa->id . "' class='fa-solid fa-caret-down'></i></p>
					<div class='mask'>
					<div id='qa_answer_" . $qa->id . "' class='text reveal-to-bottom hidden'>" . $qa->answer . "</div></div>
				</div>";
			}

			$str .= "</div></div>";
		}

		require_once($this->plugin_path . "/Templates/faq_result.php");
		FaqFrontCallbacks::myFileUploader();
		FaqFrontCallbacks::updateSiteKey();
		// FaqFrontCallbacks::reCaptchav3();
		return ob_get_clean();
	}

	public function ico_form()
	{
		ob_start(); // php ne print pas directement
		require_once($this->plugin_path . "/Templates/form_only.php");
		FaqFrontCallbacks::myFileUploader();
		FaqFrontCallbacks::updateSiteKey();
		// FaqFrontCallbacks::reCaptchav3();
		return ob_get_clean();
	}


	public function shortcodePage()
	{
		require_once($this->plugin_path . "/Templates/faq_result.php");
		require_once($this->plugin_path . "/Templates/form_only.php");
	}
}
