<?php

/**
 * @package  IconikPlugin
 */

namespace Inc\Api\Callbacks;

class MapCallbacks {
	public function addRoom() {
		$roomName   = sanitize_text_field($_POST['wp_mapping_name']);
		$roomAdr    = sanitize_text_field($_POST['wp_mapping_address']);
		$roomLink   = sanitize_text_field($_POST['wp_mapping_link']);
		$long 		= (double) $_POST['wp_mapping_long'];
		$lat  		= (double) $_POST['wp_mapping_lat'];

		global $wpdb;
		$isSuccess = $wpdb->insert(
			$wpdb->prefix . 'mapping',
			array(
				'name'      => $roomName,
				'address'   => $roomAdr,
				'link'      => $roomLink,
				'longitude' => $long,
				'latitude'  => $lat
			)
		);
		if ($isSuccess) wp_send_json_success($wpdb->insert_id);
		else wp_send_json_error($wpdb->last_error);
	}

	public function deleteRoom() {
		$id = (int) $_POST['wp_map_id'];
		global $wpdb;
		$isSuccess = $wpdb->delete(
			$wpdb->prefix . 'mapping',
			['id' => $id],
			['%d'], // id format (sécurité) 
		);

		if ($isSuccess) wp_send_json_success("success");
		else wp_send_json_error($wpdb->last_error);
	}

	public function getRoom() {
		$id = $_POST['wp_map_id'];
		global $wpdb;
		$results = $wpdb->get_results(
			$wpdb->prepare("
				SELECT * 
				FROM " . $wpdb->prefix . "mapping 
				WHERE id = " . $id
			)
		);
		if ($results) wp_send_json_success($results);
		else wp_send_json_error($wpdb->last_error);
	}

	public function editRoom() {
		$id = (int) $_POST['wp_map_id'];
		
		$roomName   = sanitize_text_field($_POST['wp_mapping_name']);
		$roomAdr    = sanitize_text_field($_POST['wp_mapping_address']);
		$roomLink   = sanitize_text_field($_POST['wp_mapping_link']);
		$long 		= (double) $_POST['wp_mapping_long'];
		$lat  		= (double) $_POST['wp_mapping_lat'];

		global $wpdb;
		
		$isSuccess = $wpdb->update($wpdb->prefix . 'mapping', 
			array('name' => $roomName, 'address' => $roomAdr, 'link' => $roomLink, 'longitude' => $long, 'latitude'  => $lat),
			array('id' => $id)
		);
		if ($isSuccess) wp_send_json_success("success");
		else wp_send_json_error($wpdb->last_error);
	}
}
