/*******Modal Bulma*******/

document.addEventListener('DOMContentLoaded', () => {
  // Functions to open and close a modal
  function openModal($el) {
    $el.classList.add('is-active');
  }

  function closeModal($el) {
    $el.classList.remove('is-active');
  }

  function closeAllModals() {
    (document.querySelectorAll('.modal') || []).forEach(($modal) => {
      closeModal($modal);
    });
  }

  // Add a click event on buttons to open a specific modal
  (document.querySelectorAll('.js-modal-trigger') || []).forEach(($trigger) => {
    const modal = $trigger.dataset.target;
    const $target = document.getElementById(modal);

    $trigger.addEventListener('click', () => {
      openModal($target);
    });
  });

  // Add a click event on various child elements to close the parent modal
  (document.querySelectorAll('.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button.is-close') || []).forEach(($close) => {
    const $target = $close.closest('.modal');

    $close.addEventListener('click', () => {
      closeModal($target);
    });
  });

  // Add a keyboard event to close all modals
  /* document.addEventListener('keydown', (event) => {
     const e = event || window.event;
 
     if (e.keyCode === 27) { // Escape key
       closeAllModals();
     }
   });*/
});


/**************************PLUGIN : MAPS*****************************
 * 
 ********************************************************************/

/** ADD Map **/
(function ($) {
  $("#add_map_form").submit(function (event) {
    let formData = {
      wp_mapping_name: $("#wp_mapping_name_add").val(),
      wp_mapping_address: $("#wp_mapping_address_add").val() + ",  " + $("#wp_mapping_address_add_cp").val() + " " + $("#wp_mapping_address_add_city").val(),
      wp_mapping_link: $("#wp_mapping_link_add").val()
    };

    getCoord(formData.wp_mapping_address).then(res => {
      if (!res.error)
        formData.wp_mapping_long = res.longt;
      formData.wp_mapping_lat = res.latt;
      const url = scriptData.ajax_url;
      $.ajax({
        url: url,
        type: 'POST',
        data: {
          ...formData,
          action: 'add_map'
        },
        error: function (error) {
          console.error(error);
        },
        success: function (response) {
          //console.log(response);
          $("#add_map_form")[0].reset(); // efface les champs
          $("#add_map_form").closest('.modal').removeClass('is-active');

          $("#sql_mapping_result").append("<tr><td class='name'>" + formData.wp_mapping_name + "</td><td class='address'>" + formData.wp_mapping_address + "</td><td class='link'>" + formData.wp_mapping_link + "</td>"
            + "<td><span id='edit-button-" + response.data + "' class='" + response.data + " icon js-modal-trigger' data-target='modal-edit-map' style='cursor: pointer;'><i class='fa-solid fa-pen'></i></span></td>"
            + "<td><span id='trash-button-" + response.data + "' class='" + response.data + " icon js-modal-trigger' data-target='modal-delete-map' style='cursor: pointer; color: #f14668;'><i class='fa-solid fa-trash'></i></span></td></tr>");

          $("#edit-button-" + response.data).click(setEditModal);
          $("#edit-button-" + response.data).click(() => { $('#modal-edit-map').addClass('is-active'); });

          $("#trash-button-" + response.data).click(setDeleteModal);
          $("#trash-button-" + response.data).click(() => { $('#modal-delete-map').addClass('is-active'); });

          bulmaToast.toast({
            message: 'La salle ' + formData.wp_mapping_name + ' a bien été ajoutée à la base de données. Vous pouvez toujours la modifier ou la supprimer.',
            type: 'is-success',
            duration: 5000,
            position: 'bottom-left',
            dismissible: 'true',
            animate: { in: 'fadeIn', out: 'fadeOut' }
          })
        }
      });
    },
      event.preventDefault());
  });

  /** DELETE Map **/
  const setDeleteModal = function (event) {
    let id = $(this).attr('class').split(' ')[0];
    $("#modal-delete-map").data("id", id);
  };

  $("[id^='trash-button']").click(setDeleteModal);

  $("#ico_delete_room").click(function () {
    let id = $("#modal-delete-map").data("id");
    const url = scriptData.ajax_url;

    $.ajax({
      type: 'POST',
      url: url,
      data: {
        wp_map_id: id,
        action: 'delete_map',
      },
      error: function (error) {
        console.error(error);
      }, success: function (response) {
        //console.log(response);
        $("#modal-delete-map").removeClass('is-active');
        $("#trash-button-" + id).closest("tr").remove();

        bulmaToast.toast({
          message: 'La salle a bien été supprimée de la base de données.',
          type: 'is-info',
          duration: 5000,
          position: 'bottom-left',
          dismissible: 'true',
          animate: { in: 'fadeIn', out: 'fadeOut' }
        })
      }
    }); // ajax delete map
  })

  /** EDIT Map **/
  const setEditModal = function (event) {
    let id_base = $(this).attr('class').split(' ')[0];
    $("#modal-edit-map").data("id", id_base);

    let id = $("#modal-edit-map").data("id");
    const url = scriptData.ajax_url;

    $.ajax({
      type: 'POST',
      url: url,
      data: {
        wp_map_id: id,
        action: 'get_map',
      },
      error: function (error) {
        console.error(error);
      }, success: function (response) {
        //console.log(response);
        const data = response.data[0];

        const fullAddr = data.address;
        const addrArray = fullAddr.split(', ');
        const cpCity = addrArray.pop().split(' ');
        const addr = addrArray.join(', ');
        const cp = cpCity.shift();
        const city = cpCity.join(' ');

        $("#wp_mapping_name_edit").val(data.name);
        $("#wp_mapping_address_edit").val(addr);
        $("#wp_mapping_address_edit_cp").val(cp);
        $("#wp_mapping_address_edit_city").val(city);
        $("#wp_mapping_link_edit").val(data.link);
      }
    });
  }

  $("[id^='edit-button-']").click(setEditModal);

  $("#modal-edit-map").submit(function (event) {
    let id = $("#modal-edit-map").data("id");
    const url = scriptData.ajax_url;

    let formData = {
      wp_map_id: id,
      wp_mapping_name: $("#wp_mapping_name_edit").val(),
      wp_mapping_address: $("#wp_mapping_address_edit").val() + ",  " + $("#wp_mapping_address_edit_cp").val() + " " + $("#wp_mapping_address_edit_city").val(),
      wp_mapping_link: $("#wp_mapping_link_edit").val()
    };

    getCoord(formData.wp_mapping_address).then(res => {
      if (!res.error)
        formData.wp_mapping_long = res.longt;
      formData.wp_mapping_lat = res.latt;
      $.ajax({
        type: 'POST',
        url: url,
        data: {
          ...formData,
          action: 'edit_map',
        },
        error: function (error) {
          console.error(error);
        }, success: function (response) {
          //console.log(response);
          $("#modal-edit-map").removeClass('is-active');

          const lineTab = $("#edit-button-" + id).closest("tr");

          lineTab.children(".name").text(formData.wp_mapping_name);
          lineTab.children(".address").text(formData.wp_mapping_address);
          lineTab.children(".link").text(formData.wp_mapping_link);


          bulmaToast.toast({
            message: 'La salle a bien été modifiée sur la base de données.',
            type: 'is-success',
            duration: 5000,
            position: 'bottom-left',
            dismissible: 'true',
            animate: { in: 'fadeIn', out: 'fadeOut' }
          })
        }
      });
    },

      event.preventDefault());
  }); // ajax edit map


  function getCoord(addr) {
    return $.get(`https://geocode.xyz/${addr.replaceAll(' ', '+')}?json=1&auth=117102282875590116684x73910`)
  }


  /************************PLUGIN : Q&A**********************
   * 
   **********************************************************************/

  /* ADD CATEGORY */

  $("#add_category_form").submit(function (event) {
    const url = scriptData.ajax_url;

    let formData = {
      wp_qa_category_name: $("#wp_qa_category_name_add").val()
    };


    $.ajax({
      type: 'POST',
      url: url,
      data: {
        ...formData,
        action: 'add_category',
      },
      error: function (error) {
        console.error(error);
      }, success: function (response) {
        console.log(response);

        $("#add_category_form")[0].reset(); // efface le champ
        $("#sql_qa_category_result").append("<tr><td class='name'>" + formData.wp_qa_category_name + " </td>"
          + "<td><span id='qa-cat-trash-button-" + response.data + "' class='" + response.data + " icon js-modal-trigger' data-target='modal-delete-category' style='cursor: pointer; color: #f14668;' ><i class='fa-solid fa-trash'></i></span></td></tr>");

        $("#qa-cat-trash-button-" + response.data).click(setDeleteModalCat);
        $("#qa-cat-trash-button-" + response.data).click(() => { $('#modal-delete-category').addClass('is-active'); });

        $("#wp_qa_category_add").append("<option value=" + response.data + ">" + formData.wp_qa_category_name + "</option>");
        $("#wp_qa_category_edit").append("<option value=" + response.data + ">" + formData.wp_qa_category_name + "</option>");


        bulmaToast.toast({
          message: 'La question a bien été ajoutée à la FAQ. Vous pouvez toujours la modifier ou la supprimer.',
          type: 'is-success',
          duration: 5000,
          position: 'bottom-left',
          dismissible: 'true',
          animate: { in: 'fadeIn', out: 'fadeOut' }
        })
      }
    });

    event.preventDefault();

  });

  /** DELETE CATEGORY **/
  const setDeleteModalCat = function (event) {
    let id = $(this).attr('class').split(' ')[0];
    $("#modal-delete-category").data("id", id);
  };

  $("[id^='qa-cat-trash-button']").click(setDeleteModalCat);

  $("#ico_delete_cat").click(function () {
    let id = $("#modal-delete-category").data("id");
    const url = scriptData.ajax_url;

    $.ajax({
      type: 'POST',
      url: url,
      data: {
        wp_category_id: id,
        action: 'delete_category',
      },
      error: function (error) {
        console.error(error);
      }, success: function (response) {
        //console.log(response);
        $("#modal-delete-category").removeClass('is-active');
        $("#qa-cat-trash-button-" + id).closest("tr").remove();

        bulmaToast.toast({
          message: 'La catégorie a bien été supprimée de la base de données.',
          type: 'is-info',
          duration: 5000,
          position: 'bottom-left',
          dismissible: 'true',
          animate: { in: 'fadeIn', out: 'fadeOut' }
        })
      }
    }); // ajax delete qa
  })


  function keepLB(str) {
    var reg = new RegExp("(%0A)", "g");
    return str.replace(reg, "%0D$1");
  }


  /* ADD Q&A */
  $("#add_qa_form").submit(function (event) {
    const url = scriptData.ajax_url;

    let formData = {
      wp_qa_question: $("#wp_qa_question_add").val(),
      wp_qa_answer: keepLB($("#wp_qa_answer_add").val()),
      wp_qa_category: $("#wp_qa_category_add").val()
    };

    $.ajax({
      type: 'POST',
      url: url,
      data: {
        ...formData,
        action: 'add_qa',
      },
      error: function (error) {
        console.error(error);
      }, success: function (response) {
        console.log(response);


        $("#all_qa_db").append("<li><br /><i class='qa-question-result'>" + formData.wp_qa_question + "</i> <button id='qa-edit-button-" + response.data + "' class='" + response.data + " js-modal-trigger button' data-target='modal-edit-qa'>Modifier</button>"
          + " <button id='qa-trash-button-" + response.data + "' class='" + response.data + " js-modal-trigger button is-danger' data-target='modal-delete-qa'>Supprimer</button><br />"
          + "→ " + formData.wp_qa_answer + "<br /><span class='tag is-info qa-category-result'>" +
          $('#wp_qa_category_add').children(':selected').text() + "</span></li>");


        $("#qa-edit-button-" + response.data).click(setEditModalQA);
        $("#qa-edit-button-" + response.data).click(() => { $('#modal-edit-qa').addClass('is-active'); });

        $("#qa-trash-button-" + response.data).click(setDeleteModalQA);
        $("#qa-trash-button-" + response.data).click(() => { $('#modal-delete-qa').addClass('is-active'); });

        $("#add_qa_form")[0].reset(); // efface les champs
        $("#add_qa_form").closest('.modal').removeClass('is-active');


        bulmaToast.toast({
          message: 'La question a bien été ajoutée à la FAQ. Vous pouvez toujours la modifier ou la supprimer.',
          type: 'is-success',
          duration: 5000,
          position: 'bottom-left',
          dismissible: 'true',
          animate: { in: 'fadeIn', out: 'fadeOut' }
        })
      }
    });
    event.preventDefault();
  });

  /* EDIT Q&A */
  const setEditModalQA = function (event) {
    let id_base = $(this).attr('class').split(' ')[0];
    $("#modal-edit-qa").data("id", id_base);

    let id = $("#modal-edit-qa").data("id");
    const url = scriptData.ajax_url;

    $.ajax({
      type: 'POST',
      url: url,
      data: {
        wp_qa_id: id,
        action: 'get_qa',
      },
      error: function (error) {
        console.error(error);
      }, success: function (response) {
        // console.log(response);
        const data = response.data[0];

        $("#wp_qa_question_edit").val(data.question);
        $("#wp_qa_answer_edit").val(data.answer);
        $("#wp_qa_category_edit").val(data.category);
      }
    });
  }

  $("[id^='qa-edit-button-']").click(setEditModalQA);

  $("#modal-edit-qa").submit(function (event) {
    let id = $("#modal-edit-qa").data("id");
    const url = scriptData.ajax_url;

    let formData = {
      wp_qa_id: id,
      wp_qa_question: $("#wp_qa_question_edit").val(),
      wp_qa_answer: $("#wp_qa_answer_edit").val(),
      wp_qa_category: $("#wp_qa_category_edit").val()
    };

    $.ajax({
      type: 'POST',
      url: url,
      data: {
        ...formData,
        action: 'edit_qa',
      },
      error: function (error) {
        console.error(error);
      }, success: function (response) {
        //console.log(response);
        $("#modal-edit-qa").removeClass('is-active');

        const lineQAEdited = $("#qa-edit-button-" + id).closest("li");
        resultCat = $('#wp_qa_category_edit').children(':selected').text();

        lineQAEdited.children(".qa-question-result").text(formData.wp_qa_question);
        lineQAEdited.children(".qa-answer-result").text(formData.wp_qa_answer);
        lineQAEdited.children(".qa-category-result").text(resultCat);

        bulmaToast.toast({
          message: 'La question a bien été modifiée sur la base de données.',
          type: 'is-success',
          duration: 5000,
          position: 'bottom-left',
          dismissible: 'true',
          animate: { in: 'fadeIn', out: 'fadeOut' }
        })
      }
    });

    event.preventDefault();
  }); // ajax edit qa



  /** DELETE QA **/
  const setDeleteModalQA = function (event) {
    let id = $(this).attr('class').split(' ')[0];
    $("#modal-delete-qa").data("id", id);
  };

  $("[id^='qa-trash-button']").click(setDeleteModalQA);

  $("#ico_delete_qa").click(function () {
    let id = $("#modal-delete-qa").data("id");
    const url = scriptData.ajax_url;

    $.ajax({
      type: 'POST',
      url: url,
      data: {
        wp_qa_id: id,
        action: 'delete_qa',
      },
      error: function (error) {
        console.error(error);
      }, success: function (response) {
        //console.log(response);
        $("#modal-delete-qa").removeClass('is-active');
        $("#qa-trash-button-" + id).closest("li").remove();

        bulmaToast.toast({
          message: 'La question a bien été supprimée de la base de données.',
          type: 'is-info',
          duration: 5000,
          position: 'bottom-left',
          dismissible: 'true',
          animate: { in: 'fadeIn', out: 'fadeOut' }
        })
      }
    }); // ajax delete qa
  })


  /**** Support API ReCaptcha V3 ****/
  $("#ico_site_key_recaptcha").on("keyup change", function (e) {
    const url = scriptData.ajax_url;
    let site_key = $("#ico_site_key_recaptcha").val();

    $.ajax({
      type: 'POST',
      url: url,
      data: {
        wp_captcha_site_key: site_key,
        action: 'update_site_key',
      },
      error: function (error) {
        console.error(error);
      }, success: function (response) {
        //console.log(response);
      }
    });
  })

  $("#ico_secret_key_recaptcha").on("keyup change", function (e) {
    const url = scriptData.ajax_url;
    let secret_key = $("#ico_secret_key_recaptcha").val();

    $.ajax({
      type: 'POST',
      url: url,
      data: {
        wp_captcha_secret_key: secret_key,
        action: 'update_secret_key',
      },
      error: function (error) {
        console.error(error);
      }, success: function (response) {
        //console.log(response);
      }
    });
  })

  $(".toggle-password").click(function () {
    $(this).children('.icon-to-toggle').toggleClass("fa-eye fa-eye-slash");
    let input = $(this).prev('input');

    if (input.attr("type") == "password")
      input.attr("type", "text");
    else input.attr("type", "password");
  });

})(jQuery);