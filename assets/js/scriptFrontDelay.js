(function ($) {

  let filesData = [];

  $("#contact_file").change(function (event) {
    filesData = [];
    const files = event.target.files;
    Array.from(files).forEach((file) => {
      if (file) {
        const reader = new FileReader();
        reader.onload = () => {
          filesData.push({
            name: file.name,
            type: file.type,
            data: reader.result,
            encoding: 'base64'
          });
        };
        reader.onerror = () => {
          console.error("Can't read the file");
        }
        reader.readAsDataURL(file)
      }
    });
  });

  $("#contact_form").submit(function (event) {
    event.preventDefault();

    const url = scriptData.ajax_url;

    grecaptcha.execute(scriptData.recaptcha_site_key, { action: 'validate_captcha' }).then(function (token) {
      // add token value to form
      document.getElementById('g-recaptcha-response').value = token;

      console.log(filesData);

      let formData = {
        'g-recaptcha-response': $("#g-recaptcha-response").val(),
        action: $("#action").val(),
        contact_obj: $("#contact_obj").val(),
        contact_file: filesData,
        contact_content: $("#contact_content").val()
      };

      $.ajax({
        type: 'POST',
        url: url,
        data: {
          ...formData,
          action: 'contact_form',
        },
        error: function (error) {
          console.error(error);
        }, success: function (response) {
          console.log(response);

          let langTag = document.querySelector("meta[property='og:locale']").getAttribute("content");
          let msg;

          if (langTag == "fr_FR")
            msg = "Votre demande a bien été prise en compte.";
          else
            msg = "Your request has been sent."

          $("#modal-contact").removeClass('is-active');

          bulmaToast.toast({
            message: msg,
            type: 'is-success',
            duration: 5000,
            position: 'bottom-center',
            dismissible: 'true',
            animate: { in: 'fadeIn', out: 'fadeOut' }
          })
        }
      });
    });
  });

  $(".flag-img").click(function () {
    $('.trp-language-switcher-container a')[0].click();
  });

})(jQuery);