/*******Modal Bulma*******/

document.addEventListener('DOMContentLoaded', () => {
  // Functions to open and close a modal
  function openModal($el) {
    $el.classList.add('is-active');
  }

  function closeModal($el) {
    $el.classList.remove('is-active');
  }

  function closeAllModals() {
    (document.querySelectorAll('.modal') || []).forEach(($modal) => {
      closeModal($modal);
    });
  }

  // Add a click event on buttons to open a specific modal
  (document.querySelectorAll('.js-modal-trigger') || []).forEach(($trigger) => {
    const modal = $trigger.dataset.target;
    const $target = document.getElementById(modal);

    $trigger.addEventListener('click', () => {
      openModal($target);
    });
  });

  // Add a click event on various child elements to close the parent modal
  (document.querySelectorAll('.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button.is-close') || []).forEach(($close) => {
    const $target = $close.closest('.modal');

    $close.addEventListener('click', () => {
      closeModal($target);
    });
  });

  // Add a keyboard event to close all modals
  /* document.addEventListener('keydown', (event) => {
     const e = event || window.event;
 
     if (e.keyCode === 27) { // Escape key
       closeAllModals();
     }
   });*/
});



function initMap(id, lat, long) {
  let map = L.map('map_' + id).setView([lat, long], 18);
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);
  L.marker([lat, long]).addTo(map);
}

/******** upload files Bulma *********/

function uploadFileBulma() {

  (function ($) {
    $(document).ready(function () {
      $("#contact_form_modal").click(function () {
        const fileInput = document.querySelector('#file-upload-js input[type=file]');
        fileInput.onchange = () => {
          if (fileInput.files.length > 0) {
            const fileName = document.querySelector('#file-upload-js .file-name');
            fileName.textContent = fileInput.files[0].name;
          }
        }


        function onSubmit(token) {
          document.getElementById("demo-form").submit();
        }
      })
    });
  })(jQuery);
}

(function ($) {
  $(document).ready(function () {

    $(".map_result_list li").click(function () {
      const city = $(this).text().split("(")[0].trim();

      $(".map_card").each(function () {
        if ($(this).data('city') == city) $(this).show();
        else $(this).hide();
      });
    });

    $("#nbMap").click(function () {
      $(".map_card").show();
    });


    /**** SUPPORT *****/

    $(".qa_question").click(function () {
      const idQA = this.id;
      let arrowIcon = $('#icon-' + idQA);

      // UI to reveal question
      if (arrowIcon.hasClass('fa-caret-down')) {
        arrowIcon.removeClass('fa-caret-down').addClass('fa-caret-left');
        $("#qa_answer_" + idQA).removeClass("hidden");
      }
      else {
        arrowIcon.addClass('fa-caret-down').removeClass('fa-caret-left');
        $("#qa_answer_" + idQA).addClass("hidden");
      }
    });

    /* plugin lang */
    $(".trp-language-switcher-container").css("display","none");

  });

})(jQuery);