<?php
/**
 * @package iconikPlugin
 */
/*
Plugin Name: Plugin Iconik
Plugin URI: https://gitlab.com/Astrid-Beyer/mapping_iconik
Description: Plug-in WP pour les sites de jeux vidéo d'Iconik. Permet de faire apparaître les salles dans lesquelles on peut retrouver un des jeux de l'entreprise.
Author: Astrid BEYER
Version: 1.0
Author URI: https://gitlab.com/Astrid-Beyer
*/

// If this file is called firectly, abort!!!
defined( 'ABSPATH' ) or die( 'Hey, what are you doing here? You silly human!' );

// Require once the Composer Autoload
if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

/**
 * The code that runs during plugin activation
 */
function activate_iconik_plugin() {
	Inc\Base\Activate::activate();
}
register_activation_hook( __FILE__, 'activate_iconik_plugin' );

/**
 * Initialize all the core classes of the plugin
 */
if ( class_exists( 'Inc\\Init' ) ) {
	Inc\Init::registerServices();
}