<script>
    <?php echo "uploadFileBulma();\n" ?>
</script>
<div class="form-help">
    <h2 class="bold">Still need us ?</h2>
    <p class="desc-form">Describe your request via Discord or send us an email.</p>

    <div class="form-help-links-contact">
        <a class='form-help-link link-form' href="https://discord.gg/iconik" target="_blank">
            <i class="fa-brands fa-discord fa-2xl"></i>
            discord.gg/iconik
        </a>

        <a id="contact_form_modal" class="js-modal-trigger form-help-link link-form" data-target="modal-contact">
            <i class="fa-solid fa-envelope fa-2xl"></i>
            Contact us
        </a>

    </div>
</div>

<div id="modal-contact" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <form id="contact_form">
            <header class="modal-card-head">
                <p class="modal-card-title">Contact us</p>
                <button class="button is-close delete" type="button"></button>
            </header>

            <section class="modal-card-body">
                <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
                <input type="hidden" id="action" name="action" value="validate_captcha">

                <div class="field is-horizontal">
                    <div class="field-label title is-4">
                        <label class="label" for="contact_obj">Subject:</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control">
                                <input id="contact_obj" name="contact_obj" class="input" type="text" placeholder="Subject of the contact request">
                            </p>
                        </div>
                    </div>
                </div>

                <div class="field">
                    <div id="file-upload-js" class="file has-name">
                        <label class="file-label">
                            <input class="file-input" type="file" name="contact_file" id="contact_file" accept="image/*" multiple="multiple">
                            <span class="file-cta">
                                <span class="file-icon">
                                    <i class="fas fa-upload"></i>
                                </span>
                                <span class="file-label">
                                    Attach one or more images...
                                </span>
                            </span>
                            <span class="file-name">
                                No file sent
                            </span>
                        </label>
                    </div>
                </div>

                <div class="field">
                    <div class="field-label">
                        <label class="label" for="contact_content">Message</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <div class="control">
                                <textarea id="contact_content" name="contact_content" class="textarea" placeholder="Formulate your request here."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer class="modal-card-foot">
                <button id="form-contact" class="button is-info" type="submit">Send</button>
                <button class="button is-close" type="button">Cancel</button>
            </footer>
        </form>
    </div>
</div>