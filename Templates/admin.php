<h1 class="title">Plugin Iconik</h1>

<h2 class="subtitle" style="padding-left:0">Bienvenue <?php echo wp_get_current_user()->display_name ?> !</h2>
<hr>

<h4 class="title is-4">API ReCaptchav3 (page support)</h4>
<p>L'API est disponible sur ce lien : <a href="https://www.google.com/recaptcha/admin/create" target="_blank">Console ReCaptcha V3</a></p>

<div class="column is-4">

    <div class="field is-horizontal">
        <div class="field-label is-normal">
            <label class="label">Clé du site</label>
        </div>
        <div class="field-body">
            <div class="field">
                <p class="control has-icons-right">
                    <input id="ico_site_key_recaptcha" class="input" type="password" placeholder="API Site Key" value="<?php echo get_option('ico_site_key_recaptcha'); ?>">
                    <span id="btn_toggle_site" class="icon is-small is-right toggle-password" style="pointer-events: initial">
                        <i id="icon-to-toggle-site" class="icon-to-toggle fas fa-eye"></i>
                    </span>
                </p>
            </div>
        </div>
    </div>

    <div class="field is-horizontal">
        <div class="field-label is-normal">
            <label class="label">Clé secrète</label>
        </div>
        <div class="field-body">
            <div class="field">
                <p class="control has-icons-right">
                    <input id="ico_secret_key_recaptcha" class="input" type="password" placeholder="API Secret Key" value="<?php echo get_option('ico_secret_key_recaptcha'); ?>">
                    <span id="btn_toggle_secret" class="icon is-small is-right toggle-password" style="pointer-events: initial">
                        <i id="icon-to-toggle-secret" class="icon-to-toggle fas fa-eye"></i>
                    </span>
                </p>
            </div>
        </div>
    </div>

</div>




<br />

<h4 class="title is-4">→ Liste des shortcodes</h4>
<ul>
    <li>
        <b>Faire apparaître les maps : </b>

        <code>[ico_mapping_result]</code>
    </li>
    <li>
        <b>Faire apparaître la FAQ : </b>

        <code>[ico_faq]</code>
    </li>
    <li>
        <b>Faire apparaître uniquement le formulaire de contact : </b>
        <code>[ico_form]</code>
    </li>
</ul>

<br/>

<h3>Développement du plugin :</h3>
<a href="https://gitlab.com/Astrid-Beyer/mapping_iconik">Voir le git</a>