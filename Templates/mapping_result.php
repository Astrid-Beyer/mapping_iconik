<div class="map_room_all">

    <?php
    foreach ($maps as $m) {
        $data = $m->address;
        $trunc = substr($data, strpos($data, ",") + 1);
        $city = trim(preg_replace('/[0-9]+/', '', $trunc));

        $str .= "<div class='map_card' data-city='$city'>";
        $str .= "<div id='map_$m->id' class='map_illustration'></div>";
        $str .= "<div class='map_info'><strong class='map_name'>" . ($m->name) . "</strong>";
        $str .= "<small class='map_address'>$m->address</small>";
        $str .= "</div>";
        $str .= "<div class='map_booking'>";
        $str .= "<button type='button' class='book_button'>";
        $str .= "<strong><a href='$m->link' target='_blank'>BOOK IT</a></strong>";
        $str .= "</button>";
        $str .= "</div>";
        $str .= "</div>";
    }
    echo $str;
    ?>
    <script>
        <?php
        foreach ($maps as $m)
            echo "initMap($m->id, $m->latitude, $m->longitude);\n";
        ?>
    </script>
</div>

<div class="results_map">
    <h1 id="nbMap">
        <?php
        $nbMap = 0;
        foreach ($maps as $m)
            ++$nbMap;
        echo $nbMap . ($nbMap < 2 ? " <span>result</span>" : " <span>results</span>");
        ?>
    </h1>
    <ul class="map_result_list p-desc">
        
    <small class="caption"><i>Click on a city above to display only rooms from this city.</i></small>
    <br/><br/>
    
        <?php
            $cities = array();
            foreach ($maps as $m) {
                $data = $m->address;
                $trunc = substr($data, strpos($data, ",") + 1);
                $city = preg_replace('/[0-9]+/', '', $trunc);

                if (!array_key_exists($city, $cities))
                    $cities[$city] = 1;
                else $cities[$city]++;
            }

            foreach($cities as $c => $num)
                echo "<li><strong class='city_result_info'>$c</strong> ($num " . ($num == 1 ? "<span>result</span>" : "<span>results</span>") . ")</li>";

        ?>
    </ul>
</div>