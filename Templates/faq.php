<div class="section">
    <div class="columns">
        <div class="column">
            <h1 class="title">Modifier la FAQ</h1>
            <button class="button is-link is-rounded js-modal-trigger" data-target="modal-add-qa">Ajouter une question</button>
            <br />
            <br />

            <div id="modal-add-qa" class="modal">
                <div class="modal-background"></div>
                <div class="modal-card">
                    <form id="add_qa_form" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
                        <header class="modal-card-head">
                            <p class="modal-card-title">Ajouter une question</p>
                            <button class="delete" aria-label="close" type="button"></button>
                        </header>

                        <section class="modal-card-body">
                            <!-- QUESTION -->
                            <div class="field is-horizontal">
                                <div class="field-label title is-5">
                                    <label class="label " for="wp_qa_question_add">Question</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control is-expanded has-icons-left">
                                            <textarea required class="input textarea" name="wp_qa_question" id="wp_qa_question_add" value=""></textarea>
                                            <span class="icon is-small is-left"><i class="fa-solid fa-circle-question"></i></span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <!-- ANSWER -->
                            <div class="field is-horizontal">
                                <div class="field-label title is-5">
                                    <label class="label " for="wp_qa_answer_add">Réponse</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control is-expanded has-icons-left">
                                            <?php $settings = array(
                                                'textarea_rows' => 7
                                            );

                                            wp_editor("", "wp_qa_answer_add", $settings); ?>
                                            <span class="icon is-small is-left"><i class="fa-solid fa-circle-exclamation"></i></span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <!-- CATEGORY -->

                            <div class="field is-horizontal">
                                <div class="field-label title is-size-5">
                                    <label class="label" for="wp_qa_category_add">Catégorie</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <select id="wp_qa_category_add" name="wp_qa_category">
                                            <?php
                                            global $wpdb;

                                            $sqlCategories = $wpdb->get_results("SELECT * FROM `$wpdb->prefix" . "qa_category`");
                                            foreach ($sqlCategories as $c)
                                                echo "<option value='$c->id'>$c->name</option>";
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <footer class="modal-card-foot">
                            <button class="button is-success" type="submit">Sauvegarder</button>
                            <button class="button is-close" type="button">Annuler</button>
                        </footer>
                    </form>
                </div>
            </div>
            <ul id="all_qa_db">

                <?php
                $sql = "
                SELECT Q.id, Q.question, Q.answer, C.name
                FROM `$wpdb->prefix" . "qa` Q 
                INNER JOIN `$wpdb->prefix" . "qa_category` C 
                ON Q.category = C.id";

                $results = $wpdb->get_results($sql);
                foreach ($results as $r) {
                    echo ("<li><br />
                    <i class='qa-question-result'>$r->question</i> <button id='qa-edit-button-" . $r->id . "' class='" . $r->id . " js-modal-trigger button' data-target='modal-edit-qa'>Modifier</button> 
                    <button id='qa-trash-button-" . $r->id . "' class='" . $r->id . " js-modal-trigger button is-danger' data-target='modal-delete-qa'>Supprimer</button><br />
                    → <span class='qa-answer-result'> $r->answer </span><br />
                    <span class='tag is-info qa-category-result'>$r->name</span>
                    </li>");
                }
                ?>
            </ul>
            <div id="modal-edit-qa" class="modal">
                <div class="modal-background"></div>
                <div class="modal-card">
                    <form id="edit_qa_form" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
                        <header class="modal-card-head">
                            <p class="modal-card-title">Modifier une question</p>
                            <button class="delete" aria-label="close" type="button"></button>
                        </header>

                        <section class="modal-card-body">
                            <!-- QUESTION -->
                            <div class="field is-horizontal">
                                <div class="field-label title is-5">
                                    <label class="label " for="wp_qa_question_edit">Question</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control is-expanded has-icons-left">
                                            <textarea required class="input textarea" name="wp_qa_question" id="wp_qa_question_edit" value=""></textarea>
                                            <span class="icon is-small is-left"><i class="fa-solid fa-circle-question"></i></span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <!-- ANSWER -->
                            <div class="field is-horizontal">
                                <div class="field-label title is-5">
                                    <label class="label " for="wp_qa_answer_edit">Réponse</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control is-expanded has-icons-left">
                                            <?php
                                            $settings = array(
                                                'textarea_rows' => 7
                                            );
                                            wp_editor("", "wp_qa_answer_edit", $settings); ?>
                                            <span class="icon is-small is-left"><i class="fa-solid fa-circle-exclamation"></i></span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <!-- CATEGORY -->

                            <div class="field is-horizontal">
                                <div class="field-label title is-size-5">
                                    <label class="label" for="wp_qa_category_edit">Catégorie</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <select id="wp_qa_category_edit" name="wp_qa_category">
                                            <?php
                                            global $wpdb;

                                            $sqlCategories = $wpdb->get_results("SELECT * FROM `$wpdb->prefix" . "qa_category`");
                                            foreach ($sqlCategories as $c)
                                                echo "<option value='$c->id'>$c->name</option>";
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <footer class="modal-card-foot">
                            <button class="button is-success" type="submit">Sauvegarder</button>
                            <button class="button is-close" type="button">Annuler</button>
                        </footer>
                    </form>
                </div>
            </div>

            <div id="modal-delete-qa" class="modal">
                <div class="modal-background"></div>
                <div class="modal-card">
                    <header class="modal-card-head">
                        <p class="modal-card-title">Supprimer une question</p>
                        <button class="delete" aria-label="close"></button>
                    </header>
                    <section class="modal-card-body">
                        <strong>Attention !</strong> Cette action va supprimer une question de la base de données.
                        Elle ne s'affichera plus sur la page FAQ du site.
                        Confirmez-vous que vous souhaitez supprimer cette question ?
                    </section>
                    <footer class="modal-card-foot">
                        <button id="ico_delete_qa" class="button is-danger">Je confirme</button>
                        <button class="button is-close">Annuler</button>
                    </footer>
                </div>
            </div>


        </div>
        <div class="column">
            <h1 class="title is-4">Ajouter une catégorie de question</h1>
            <form id="add_category_form" class="field is-grouped">
                <p class="control is-expanded">
                    <input required id="wp_qa_category_name_add" name="wp_qa_category_name" class="input" type="text" placeholder="Nom de catégorie">
                </p>
                <p class="control">
                    <button class="button is-info" type="submit">
                        Ajouter
                    </button>
                </p>
            </form>
            <h1 class="title is-4">Catégories existantes :</h1>
            <table class="table is-striped is-hoverable is-narrow is-bordered">
                <tbody id="sql_qa_category_result">
                    <?php
                    $categories = $wpdb->get_results("SELECT * FROM `wp_qa_category`");
                    foreach ($categories as $c) {
                        echo ("<tr><td class='name'>" . $c->name . " </td>
                        <td><span id='qa-cat-trash-button-" . $c->id . "' class='" . $c->id . " icon js-modal-trigger' data-target='modal-delete-category' style='cursor: pointer; color: #f14668;' ><i class='fa-solid fa-trash'></i></span></td>
                        </tr>");
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div id="modal-delete-category" class="modal">
            <div class="modal-background"></div>
            <div class="modal-card">
                <header class="modal-card-head">
                    <p class="modal-card-title">Supprimer une catégorie</p>
                    <button class="delete" aria-label="close"></button>
                </header>
                <section class="modal-card-body">
                    <strong>Attention !</strong> Cette action va supprimer une catégorie de la base de données.
                    Elle ne sera plus disponible pour renseigner une nouvelle question sur ce sujet.
                    Confirmez-vous que vous souhaitez supprimer cette catégorie ?
                </section>
                <footer class="modal-card-foot">
                    <button id="ico_delete_cat" class="button is-danger">Je confirme</button>
                    <button class="button is-close">Annuler</button>
                </footer>
            </div>
        </div>
    </div>
</div>