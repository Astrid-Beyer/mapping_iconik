<h1 class="title">Gérer le plug-in sur la page Map</h1>
<button class="button is-link is-rounded js-modal-trigger" data-target="modal-add-map">Ajouter une salle</button>
<br/>
<br/>
<div id="modal-add-map" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <form id="add_map_form" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
            <header class="modal-card-head">
                <p class="modal-card-title">Ajouter une salle</p>
                <button class="delete" aria-label="close" type="button"></button>
            </header>
            <section class="modal-card-body">
                <!-- NOM -->
                <div class="field is-horizontal">
                    <div class="field-label title is-5">
                        <label class="label " for="wp_mapping_name_add">Nom</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control is-expanded">
                                <input required type="text" class="input" name="wp_mapping_name" id="wp_mapping_name_add" value="" placeholder="Nom de la salle" />
                        </div>
                    </div>
                </div>

                <!-- ADRESSE -->
                <div class="field is-horizontal">
                    <div class="field-label title is-5">
                        <label class="label " for="wp_mapping_address_add">Adresse</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control is-expanded has-icons-left"> 
                                <input required class="input" type="text" name="wp_mapping_address" id="wp_mapping_address_add" value="" placeholder="Adresse" />
                                <span class="icon is-small is-left">
                                    <i class="fa-solid fa-location-dot"></i>
                                </span>
                            </p>
                        </div>
                        <div class="field">
                            <p class="control is-expanded has-icons-left">
                                <input required class="input" type="text" name="wp_mapping_address_cp" id="wp_mapping_address_add_cp" value="" placeholder="Code postal">
                                <span class="icon is-small is-left">
                                    <i class="fa-solid fa-location-arrow"></i>
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label"></div>
                    <div class="field-body">
                        <div class="field is-expanded">
                            <div class="field">
                                <p class="control is-expanded has-icons-left">
                                    <input required class="input" type="text" name="wp_mapping_address_city" id="wp_mapping_address_add_city" value="" placeholder="Ville">
                                    <span class="icon is-small is-left">
                                        <i class="fa-solid fa-city"></i>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- LIEN -->
                <div class="field is-horizontal">
                    <div class="field-label title is-5">
                        <label class="label " for="wp_mapping_link_add">Lien</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <div class="field">
                                <p class="control is-expanded has-icons-left">
                                    <input required type="text" class="input is-normal" name="wp_mapping_link" id="wp_mapping_link_add" value="" placeholder="Site Internet" /><br /><br /><span class="icon is-small is-left">
                                        <i class="fa-solid fa-link"></i>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            <footer class="modal-card-foot">
                <button class="button is-success" type="submit">Sauvegarder</button>
                <button class="button is-close" type="button">Annuler</button>
            </footer>
        </form>
    </div>
</div>


<div id="modal-delete-map" class="modal">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Supprimer une salle</p>
      <button class="delete" aria-label="close"></button>
    </header>
    <section class="modal-card-body">
      <strong>Attention !</strong> Cette action va supprimer une salle de la base de données. Elle ne s'affichera plus sur la page Map du site.
      Confirmez-vous que vous souhaitez supprimer cette salle ?
    </section>
    <footer class="modal-card-foot">
      <button id="ico_delete_room" class="button is-danger">Je confirme</button>
      <button class="button is-close">Annuler</button>
    </footer>
  </div>
</div>

<div id="modal-edit-map" class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <form id="edit_map_form" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
            <header class="modal-card-head">
                <p class="modal-card-title">Modifier une salle</p>
                <button class="delete" aria-label="close" type="button"></button>
            </header>
            <section class="modal-card-body">
                <!-- NOM -->
                <div class="field is-horizontal">
                    <div class="field-label title is-5">
                        <label class="label " for="wp_mapping_name_edit">Nom</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control is-expanded">
                                <input required type="text" class="input" name="wp_mapping_name" id="wp_mapping_name_edit" value="" placeholder="Nom de la salle" />
                        </div>
                    </div>
                </div>

                <!-- ADRESSE -->
                <div class="field is-horizontal">
                    <div class="field-label title is-5">
                        <label class="label " for="wp_mapping_address_edit">Adresse</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <p class="control is-expanded has-icons-left">
                                <input required class="input" type="text" name="wp_mapping_address" id="wp_mapping_address_edit" value="" placeholder="Adresse" />
                                <span class="icon is-small is-left">
                                    <i class="fa-solid fa-location-dot"></i>
                                </span>
                            </p>
                        </div>
                        <div class="field">
                            <p class="control is-expanded has-icons-left">
                                <input required class="input" type="text" name="wp_mapping_address_cp" id="wp_mapping_address_edit_cp" value="" placeholder="Code postal">
                                <span class="icon is-small is-left">
                                    <i class="fa-solid fa-location-arrow"></i>
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label"></div>
                    <div class="field-body">
                        <div class="field is-expanded">
                            <div class="field">
                                <p class="control is-expanded has-icons-left">
                                    <input required class="input" type="text" name="wp_mapping_address_city" id="wp_mapping_address_edit_city" value="" placeholder="Ville">
                                    <span class="icon is-small is-left">
                                        <i class="fa-solid fa-city"></i>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- LIEN -->
                <div class="field is-horizontal">
                    <div class="field-label title is-5">
                        <label class="label " for="wp_mapping_link_edit">Lien</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <div class="field">
                                <p class="control is-expanded has-icons-left">
                                    <input required type="text" class="input is-normal" name="wp_mapping_link" id="wp_mapping_link_edit" value="" placeholder="Site Internet" /><br /><br /><span class="icon is-small is-left">
                                        <i class="fa-solid fa-link"></i>
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
            <footer class="modal-card-foot">
                <button id="ico_edit_room" class="button is-info" type="submit">Modifier</button>
                <button class="button is-close" type="button">Annuler</button>
            </footer>
        </form>
    </div>
</div>



<table class="table is-striped">

    <thead>
        <tr>
            <th>Nom de la salle</th>
            <th>Adresse</th>
            <th>Site web</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody id="sql_mapping_result">

        <?php
        $sql = "SELECT * FROM `wp_mapping`";
        global $wpdb;
        $results = $wpdb->get_results($sql);
        foreach ($results as $r) {
            echo ("<tr>
            <td class='name'>" . $r->name . "</td>
            <td class='address'>" . $r->address . "</td>
            <td class='link'>" . $r->link . "</td>
            <td><span id='edit-button-" . $r->id . "' class='" . $r->id . " icon js-modal-trigger' data-target='modal-edit-map' style='cursor: pointer;'><i class='fa-solid fa-pen'></i></span></td>
            <td><span id='trash-button-" . $r->id . "' class='" . $r->id . " icon js-modal-trigger' data-target='modal-delete-map' style='cursor: pointer; color: #f14668;' ><i class='fa-solid fa-trash'></i></span></td>
            </tr>");
        } ?>
    </tbody>
</table>