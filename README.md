# Mapping Iconik

Plug-in WP (WordPress) pour les sites de jeux vidéo d'Iconik

Pour faire fonctionner le plug-in côté dev, ne pas oublier d'utiliser la commande suivante :
`composer install`

## Utilisation

Lorsque le plug-in est activé, un nouveau menu "Iconik" apparaît dans la marge du panneau d'administration WP. Ce menu comporte trois pages.

### Shortcodes

• Shortcode à insérer sur une page pour faire apparaître le plug-in map :

```[ico_mapping_result]```

Activer le plug-in va instancier une nouvelle base de données `wp_mapping` vide sur le site. Il est possible d'y ajouter des salles via le panneau dédié sur l'administration du plug-in directement sur WordPress en tant qu'admin. Désactiver le plug-in ne va pas supprimer la base de données (au cas où une mauvaise manipulation est faite les données ne seront pas perdues).

Mais s'il est souhaité de vider et supprimer la table, cela reste possible sur l'interface PhpMyAdmin qui devrait être accessible avec un compte administrateur du serveur de base de données.


• Shortcode pour faire apparaître la foire aux questions et le formulaire de contact :

```[ico_faq]```

Une base de données de **questions** et de **catégories de questions** sont instanciées lors de l'activation du plug-in : `wp_qa` et `wp_qa_category`. Depuis le panneau d'administration WP, il est possible de renseigner des catégories de question ou les supprimer. Il en existe 5 de bases (Général, Quest, PlayStationVR...). Il est ensuite possible de renseigner une question & sa réponse ainsi que la catégorie de question, modifier et supprimer.

L'outil permettant de renseigner les réponses aux questions comprend le HTML, on peut donc y glisser des liens, écrire en gras ect.

• Shortcode pour faire apparaître uniquement le formulaire de contact :

```[ico_form]```

---

### Page Dashboard

La page principale recense la liste des shortcodes (éléments instanciables sur une page du site) et permet de saisir les clés pour l'API ReCaptchaV3.

Cette API est utile pour éviter le spam lors de l'utilisation du formulaire de contact. Elle n'est utilisée que lorsqu'un shortcode comportant un formulaire de contact est présent sur une page.

### Page Map Manager

Dans cette page, l'administrateur a la possibilité de renseigner des salles d'arcades VR dans lesquelles le jeu est disponible.

Le bouton "Ajouter une salle" permet de remplir la base de données `wp_mapping` du site. Depuis cette page, on peut modifier et/ou supprimer une entrée de la base de données.

Lorsqu'une action (ajout, édition, suppression) est réalisée, un message apparaît pour confirmer que l'action a bien été appliquée en base de données.

### Page FAQ

Similaire à la page de renseignement de salles, cette page permet d'ajouter, modifier et/ou supprimer une question. Attention à bien définir les catégories de question avant d'entrer des questions.

/!\ Ne pas supprimer une catégorie AVANT de supprimer les questions comportant la catégorie à supprimer. Cette exception n'est pas gérée et risque de causer une erreur.

Lorsqu'une action a bien été enregistrée en base de données, un message de confirmation sera visible.


## Informations complémentaires

**Framework CSS** : Bulma (https://bulma.io/documentation/)

API utilisée pour l'affichage des cartes : https://leafletjs.com/reference-0.7.7.html

Politique de l'API : https://operations.osmfoundation.org/policies/tiles/

→ API pour chercher les adresses des salles avant d'afficher les cartes (et obtenir les coordonnées) : https://geocode.xyz/

API YouTube V3

API Google ReCaptcha V3

---

Plug-in réalisé avec ♥ pour Iconik© par Astrid BEYER, 2022
